create table quote (
	symbol varchar(10) NOT NULL,
	date date NOT NULL,
	price_opening double precision NOT NULL,
	price_closing double precision NOT NULL,
	price_closing_adjusted double precision NOT NULL,
	price_high double precision NOT NULL,
	price_low double precision NOT NULL,
	volume bigint NOT NULL,
	PRIMARY KEY(symbol, date)
);

create index IDX_QUOTE_SYMBOL on quote(symbol);
create index IDX_QUOTE_SYMBOL_DATE_SORT on quote(symbol, date ASC);


create table sentiment (
	symbol varchar(10) NOT NULL,
	date date NOT NULL,
	category varchar(20) NOT NULL,
	post_count int NOT NULL,
	nltk_positive double precision NULL,
	nltk_negative double precision NULL,
	nltk_netral double precision NULL,
	flair_positive double precision NULL,
	flair_negative double precision NULL,
	textblob_polarity double precision NULL,
	textblob_subjectivity double precision NULL,
	PRIMARY KEY(symbol, date, category)
);

create index IDX_SENTIMENT_SYMBOL on sentiment(symbol);
create index IDX_SENTIMENT_SYMBOL_DATE_SORT on sentiment(symbol, date ASC);
create index IDX_SENTIMENT_SYMBOL_CATEGORY_DATE_SORT on sentiment(symbol, category ASC, date ASC);